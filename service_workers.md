###### sources 
* https://codeburst.io/an-introduction-to-service-workers-in-javascript-27d6376460c2
* https://developers.google.com/web/fundamentals/primers/service-workers/
* https://developer.mozilla.org/en-US/docs/Learn/Server-side/Node_server_without_framework
* 

###### ideas
* service workers
* promises
* es6?
* node?
* push notifications
* background syncs
* offline experiences
* postMessage interface
* indexedDB API

#### basics
A service worker is a script that your browser runs in the background, separate from a web page, opening the door to features that don't need a web page or user interaction

A Service worker is basically a script (JavaScript file) that runs in background and assists in offline first web application development

A Service worker can not directly interact with the webpage 
 
Nor can it directly access the DOM 

can directly access the DOM

but can communicate with webpages through messages (Post Messages).

fully asynchronous

which excludes local storage and synchronous XHR

Service workers are terminated when not in use and restored when required. 

It acts as a programmable network proxy, allowing developers to handle how network requests from the web page is handle

So the developers can take appropriate action based on the availability of network.

Note that if you have never used promises before, better read about them first as service workers use promises intensively.

_Service Worker v/s Web Worker_

Both Service workers and Web workers are JavaScript workers with lots of similarities but there are a few difference too.

Service workers are designed to handle network requests and assist in offline first development, Push Notifications and background syncs. Communication’s with the webpage must go through service workers PostMessage method.

communications with the wepage must go through service workers postMessage method

Web workers mimics the multi-threading model, allowing complex / data or processor intensive tasks to run in background.

Service workers can be registered on each page load without concern, the browser will handle, if the registration is required or not.#

Registration life cycle consists of 3 steps:

0. Download
0. Install
0. Activate

you cannot rely on global state within a service worker's onfetch and onmessage handlers. If there is information that you need to persist and reuse across restarts, service workers do have access to the IndexedDB API.

A service worker has a lifecycle that is completely separate from your web page.

Typically during the install step, you'll want to cache some static assets. If all the files are cached successfully, then the service worker becomes installed

Service workers are supported by Chrome, Firefox and Opera. Microsoft Edge is now showing public support. Even Safari has dropped hints of future development.

Using service worker you can hijack connections, fabricate, and filter responses

##### questions
0. what is a webworker
0. 
