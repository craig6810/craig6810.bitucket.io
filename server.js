const http = require('http');
const fs = require('fs'); // api for interacting with the filesystem

const hostname = '0.0.0.0';
const port = 3000;

const server = http.createServer((req, res) => {
    let contentType = 'text/html';
    let filePath = './';

    switch (req.url) {
        case '/app.js':
            filePath += 'app.js';
            contentType = 'text/javascript';
            break;
        case '/sw.js':
            filePath += 'sw.js';
            contentType = 'text/javascript';
            break;
        case '.':
        default:
            filePath += 'index.html';
    }

    fs.readFile(filePath, (err, content) => {
        if (err) {
            console.log('Error: ' .err.message);
        } else {
          res.writeHead(200, { 'Content-Type': contentType});
          res.end(content, 'utf-8');
        }
    });
});

server.listen(port, hostname);